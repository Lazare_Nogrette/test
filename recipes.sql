

CREATE TABLE user IF NOT EXISTS user(
id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
username VARCHAR(50) UNIQUE,
email VARCHAR(50)
);

CREATE TABLE recipes(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    title VARCHAR(50) NOT NULL,
    slug VARCHAR(50) NOT NULL,
    date DATETIME,
    duration INTEGER DEFAULT 0,
    user_id INTEGER NOT NULL
);

CREATE TABLE categories(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    title VARCHAR(50) NOT NULL
);

CREATE TABLE categories_recipes (
    recipe_id INTEGER NOT NULL,
    category_id INTEGER NOT NULL,
    FOREIGN KEY (recipe_id) REFERENCES recipes(id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE, 
    PRIMARY KEY (recipe_id, category_id),
    UNIQUE (recipe_id, category_id)
);