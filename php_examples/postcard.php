<?php
namespace App\Table;
use App\{Router, Connection, Post, PaginatedQuery};

abstract class PostTable{

    private $pdo;

    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function findPaginated() {
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM products",
            "SELECT COUNT(id) FROM products",
            "SELECT * FROM products WHERE id = .id",
            $this->pdo
        );
    }
}

class CategoryTable extends PostTable {

    private $pdo;

    public function __construct(\PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function find(int $id): CategoryTable
    {
        $query = $this->pdo->prepare('SELECT * FROM category WHERE id =.id');
        $query->execute(['id' => $id]);
        $query->setFetchMode(\PDO::FETCH_CLASS, Category::class);
        return $query->fetch();
    }
}