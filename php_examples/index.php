<?php

use App\Router;
use App\{Connection, Post, PaginatedQuery};

require 'router.php';
require 'connection.php';

$router = new Router(dirname(__DIR__));

$pdo = Connection::getPDO();

$posts = paginatedQuery::getItems();
$currentPage = 2;
$offset = 12 * ($currentPage - 1);
$paginatedQuery = new PaginatedQuery(
    "SELECT p.*
    FROM products p
    LIMIT 12 OFFSET $offset",
    "SELECT * FROM products WHERE id = .id",
    Post::class
);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a href="#" class="navbar-brand">Site</a>
    </nav>
    <h1>Blog</h1>

    <div class="row">
        <?php foreach($posts as $post): ?>
        <div class="col-md-3">
            <div class="card">
                <div class="card-body">
                    <h5><?= htmlentities($post->name) ?></h5>
                </div>
            </div>

        </div>
        <?php endforeach ?>
    </div>
    <div class="d-flex justify-content-between my-4">
    </div>
</body>
</html>