<?php

namespace App;

class Auth{
    private $pdo;
    
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function user()
    {

    }

    public function login(string $nickname, string $password)
    {
        $query = $this->pdo->prepare('SELECT * FROM users WHERE username = :username');
        $query->execute(['username' => $nickname]);
        $query->setFetchMode(\PDO::FETCH_CLASS, User::class);
        $user = $query->fetch();
        if ($user !== false)
        {
            if (password_verify($password, $user->password))
            {
                if (session_status() === PHP_SESSION_NONE)
                {
                    session_start();
                }
                $_SESSION['auth'] = $user->id;
                return $user;
            }
        }
        return null;

    }
}