<?php
namespace App;

use PDO;

class Connection{
    public static function getPDO(): PDO {
        return new PDO("sqlite:./php_examples/products.db", null, null, [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
    }
}