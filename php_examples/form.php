<?php

namespace App\HTML;

$form = new Form($post, $errors);

class Form {
    private $data;
    private $errors;

    public function __construct($data, array $errors)
    {
        $this->data = $data;
        $this->errors = $errors;
    }

    public function input(string $name, string $label): string
    {
        $value = $this->data->getName();
        return <<<HTML
        <div class="form-group">
            <label for="field{$name}">{$label}</label>
            <input type="text" id="field{$name}" class="form-control" name="{$name}" value="" required>
            <?php if (isset($errors['name'])): ?>
            <div class="invalid-feedback">
                <?= implode('<br>', $errors['name'])?>;
            </div>
            <?php endif ?>
        </div>
        HTML;
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editer l'article</title>
</head>
<body>
    <h1>Editer l'article <?= e($post->getName())?></h1>

    <form action="" method="POST">
        <?= $form->input('name', 'Titre'); ?>
        <?= $form->input('slug', 'URL'); ?>
        <?= $form->input('content', 'Contenu'); ?>
        <button class="btn btn-primary">Modifier</button>
    </form>
    
</body>
</html>
