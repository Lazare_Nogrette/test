<?php 
function add_views(string $file) : void
{
    $count = 1;
    if (file_exists($file)){
        $count = file_get_contents($file);
        $count++;
    }
    file_put_contents($file, $count);
}

function get_views(): string {
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'count';
    return file_get_contents($file);
}

function display_views(): void {
    $file = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'count';
    add_views($file);
}

function is_connected(): bool {
    if(session_status === PHP_SESSION_NONE)
    {
        session_start();
    }
    return !empty($_SESSION['connected']);
}

class Message{

    private $nickname;
    private $message;

    public function __construct(string $nickname, string $message)
    {
        $this->nickname = $nickname;
        $this->message = $message;
    }

    public function toHTML(): string 
    {
        $nickname = htmlentities($this->username);
        return <<<HTML
            <strong>$nickname</strong>
        HTML;
    }
}

class Book{

    private $file;

    public function __construct(string $file)
    {
        $directory = dirname($file);
        if (!is_dir($directory))
        {
            mkdir($directory, 0777, true);
        }
        if (!file_exists($file)){
            touch($file);
        }
        $this->file = $file;
    }
}