<?php 
$pdo = new PDO("sqlite:./products.db", null, null, [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);

$page = (int)($_GET['p'] ?? 1);
$prods = $pdo->query('SELECT * FROM products')->fetchAll();
/*var_dump($prods);*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mobiliers</title>
</head>
<body>
    <table class="tabled">
        <thead>
       <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>City</th>
            <th>Address</th>
       </tr>
        </thead>
        <tbody>

        <?php foreach($prods as $product): ?>
            <tr>
                <td>#<?= $product['id'] ?></td>
                <td><?= $product['name'] ?></td>
                <td><?= $product['price'] ?>$</td>
                <td><?= $product['city'] ?></td>
                <td><?= $product['address'] ?></td>
            </tr>
           <?php endforeach ?>
        </tbody>
    </table>

    <?php if ($pages > 1 && $page <$pages): ?>
            <a href="?p=<?= $page + 1 ?>">Next page</a>
    <?php endif ?>
</body>
</html>