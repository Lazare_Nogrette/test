<?php
namespace App;

class QueryBuilder{
    private $from;
    private $order;
    private $limit;
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function from(string $table, string $alias)
    {
        $this->from = "$table $alias";
        return $this->from;
    }

    public function orderBy(string $key, string $direction){
        $this->order[] = "$key $direction";
        return $this;
    }

    public function ToSQL(): string {
        $sql= "SELECT * FROM $this->from";
        if ($this->limit > 0)
        {
            $sql .= "LIMIT " . $this->limit;
        }
        return $sql;
    }
}