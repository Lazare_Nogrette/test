<?php
namespace App;

use DateTime;
use PDO;
use Exception;

class Router{

    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }
}

class Post {

    private $id;
    private $name;
    private $content;
    private $created_at;
    private $categories = [];
    public $slug;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCreatedAt(): DateTime
    {
        return new DateTime($this->created_at);
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function getID(): ?int
    {
        return $this->id;
    }

    public function getExcerpt(): ?string
    {
        if ($this->content === null)
        {
            return null;
        }
        /*return nl2br(htmlentities(Text::excerpt($this->content, 60)));*/
    }

    public function getFormattedContent(): ?string{
        return nl2br($this->content);
    }
}

class PaginatedQuery {

    private $query;
    private $queryCount;
    private $classMapping;
    private $pdo;
    private $perPage;

    public function __construct(
        string $query, string $queryCount,
        string $classMapping, ?\PDO $pdo = null,
        int $perPage = 12
    )
    {
        $this->query = $query;
        $this->queryCount = $queryCount;
        $this->classMapping = $classMapping;
        $this->pdo = $pdo ?: Connection::getPDO();
        $this->perPage = $perPage;
    }

    public static function getItems(): array
    {
        $pdo = Connection::getPDO();
        $page = (int)($_GET['page'] ?? 1) ?: 1;
        if (!filter_var($page, FILTER_VALIDATE_INT)){
            throw new Exception("Invalid page number");
            }
            $curPage = (int)$page;
            $count = (int)$pdo->query('SELECT COUNT(id) FROM products')->fetch(PDO::FETCH_NUM)[0];
            $query = $pdo->query('SELECT * FROM products DESC LIMIT 20');
        return $pdo->$query->fetchAll(PDO::FETCH_CLASS);
    }
}